module gitlab.com/gitlab-org/gitlab-elasticsearch-indexer

go 1.16

require (
	github.com/aws/aws-sdk-go v1.27.0
	github.com/deoxxa/aws_signing_client v0.0.0-20161109131055-c20ee106809e
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-enry/go-enry/v2 v2.7.1
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/olivere/elastic v6.2.24+incompatible
	github.com/sirupsen/logrus v1.7.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/gitlab-org/gitaly v1.68.0
	gitlab.com/gitlab-org/labkit v1.3.1
	gitlab.com/lupine/icu v1.0.0
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	golang.org/x/tools v0.1.0
	google.golang.org/grpc v1.35.0
)
